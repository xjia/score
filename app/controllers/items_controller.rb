class ItemsController < ApplicationController
  before_filter :find_catalog

  def index
    respond_to do |format|
      format.html { redirect_to @catalog }
      format.json { render json: @catalog.items }
    end
  end

  def show
    respond_to do |format|
      format.html { redirect_to @catalog }
      format.json { render json: Item.find(params[:id]) }
    end
  end

  def new
    @item = Item.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @item }
    end
  end

  def edit
    @item = Item.find(params[:id])
  end

  def create
    @item = Item.new(params[:item])
    @item.catalog = @catalog

    respond_to do |format|
      if @item.save
        format.html { redirect_to @catalog, notice: 'Item was successfully created.' }
        format.json { render json: @item, status: :created, location: @item }
      else
        format.html { render action: "new" }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @item = Item.find(params[:id])
    @item.catalog = @catalog

    respond_to do |format|
      if @item.update_attributes(params[:item])
        format.html { redirect_to @catalog, notice: 'Item was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @item = Item.find(params[:id])
    @item.destroy

    respond_to do |format|
      format.html { redirect_to @catalog, notice: 'Item was successfully removed.' }
      format.json { head :no_content }
    end
  end

protected
  def find_catalog
    @catalog = Catalog.find(params[:catalog_id])
  end
end
