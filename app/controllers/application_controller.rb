class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :authenticate_user!

protected
  def authenticate_user!
    @current_user ||= User.first
  end

  def current_user
    @current_user
  end
end
