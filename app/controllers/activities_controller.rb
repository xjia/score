class ActivitiesController < ApplicationController
  before_filter :find_activity, except: [:index, :new, :create]

  # GET /activities
  # GET /activities.json
  def index
    @activities = Activity.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @activities }
    end
  end

  # GET /activities/1
  # GET /activities/1.json
  def show
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @activity }
    end
  end

  # GET /activities/new
  # GET /activities/new.json
  def new
    @activity = Activity.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @activity }
    end
  end

  # GET /activities/1/edit
  def edit
    respond_to do |format|
      format.html
      format.json { render json: @activity }
    end
  end

  # POST /activities
  # POST /activities.json
  def create
    @activity = Activity.new(params[:activity])
    @activity.user = current_user
    @activity.catalog = Catalog.find(params[:activity][:catalog_id])

    respond_to do |format|
      if @activity.save
        format.html { redirect_to activities_url, notice: 'Activity was successfully created.' }
        format.json { render json: @activity, status: :created, location: @activity }
      else
        format.html { render action: "new" }
        format.json { render json: @activity.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /activities/1
  # PUT /activities/1.json
  def update
    @activity.user = current_user

    respond_to do |format|
      if @activity.update_attributes(params[:activity])
        format.html { redirect_to activities_url, notice: 'Activity was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @activity.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /activities/1
  # DELETE /activities/1.json
  def destroy
    @activity.destroy

    respond_to do |format|
      format.html { redirect_to activities_url, notice: 'Activity was successfully removed.' }
      format.json { head :no_content }
    end
  end

  def review
    respond_to do |format|
      format.html
      format.json { head :no_content }
    end
  end

  def submit_review
    Grade.transaction do
      @activity.participations.each do |part|
        Grade.delete_all(user_id: current_user.id, participation_id: part.id)
        @activity.catalog.items.each do |item|
          name = "p#{part.id}i#{item.id}"
          grade = Grade.new
          grade.user = current_user
          grade.participation = part
          grade.item = item
          grade.score = params[name]
          grade.save
        end
      end
    end

    respond_to do |format|
      format.html { redirect_to activities_url, notice: 'Your review was successfully updated.' }
      format.json { head :no_content }
    end
  end

  def scoreboard
  end

protected
  def find_activity
    @activity = Activity.find(params[:id])
  end
end
