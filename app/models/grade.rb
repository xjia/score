class Grade < ActiveRecord::Base
  belongs_to :user
  belongs_to :participation
  belongs_to :item
  attr_accessible :score

  def self.find_or_max(user, participation, item)
    if grade = Grade.where(user_id: user.id, participation_id: participation.id, item_id: item.id).first
      grade.score
    else
      item.max_limit
    end
  end
end
