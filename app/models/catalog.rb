class Catalog < ActiveRecord::Base
  belongs_to :user
  has_many :items, order: 'priority DESC'
  has_many :activities
  attr_accessible :title

  def to_s
    title
  end
end
