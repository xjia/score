class Participation < ActiveRecord::Base
  belongs_to :user
  belongs_to :activity
  has_many :grades
  attr_accessible :attachment_url, :description, :display_name
end
