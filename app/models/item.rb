class Item < ActiveRecord::Base
  belongs_to :catalog
  has_many :grades
  attr_accessible :description, :max_limit, :min_limit, :title, :priority

  def options
    (min_limit..max_limit).to_a
  end
end
