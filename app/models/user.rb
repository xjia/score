class User < ActiveRecord::Base
  has_many :catalogs
  has_many :activities
  has_many :participations
  has_many :grades
  attr_accessible :login_name

  def to_s
    login_name
  end

  def participated_in(activity)
    participations.where(activity_id: activity.id).first
  end

  def is_owner_of(something)
    id == something.try(&:user_id)
  end
end
