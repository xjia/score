class Activity < ActiveRecord::Base
  belongs_to :user
  belongs_to :catalog
  has_many :participations
  has_many :grades, through: :participations
  attr_accessible :is_public, :title, :catalog_id

  def to_s
    title
  end

  def reviewers
    grades.collect(&:user_id).uniq
  end
end
