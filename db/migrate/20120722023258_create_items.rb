class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.references :catalog
      t.string :title
      t.text :description
      t.integer :min_limit
      t.integer :max_limit

      t.timestamps
    end
    add_index :items, :catalog_id
  end
end
