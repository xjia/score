class CreateCatalogs < ActiveRecord::Migration
  def change
    create_table :catalogs do |t|
      t.references :user
      t.string :title

      t.timestamps
    end
    add_index :catalogs, :user_id
  end
end
