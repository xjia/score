class CreateParticipations < ActiveRecord::Migration
  def change
    create_table :participations do |t|
      t.references :user
      t.references :activity
      t.string :display_name
      t.text :description
      t.string :attachment_url

      t.timestamps
    end
    add_index :participations, :user_id
    add_index :participations, :activity_id
  end
end
