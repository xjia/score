class CreateGrades < ActiveRecord::Migration
  def change
    create_table :grades do |t|
      t.references :user
      t.references :participation
      t.references :item
      t.integer :score

      t.timestamps
    end
    add_index :grades, :user_id
    add_index :grades, :participation_id
    add_index :grades, :item_id
  end
end
