class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.references :user
      t.string :title
      t.references :catalog
      t.boolean :is_public

      t.timestamps
    end
    add_index :activities, :user_id
    add_index :activities, :catalog_id
  end
end
