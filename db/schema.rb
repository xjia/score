# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120722024417) do

  create_table "activities", :force => true do |t|
    t.integer  "user_id"
    t.string   "title"
    t.integer  "catalog_id"
    t.boolean  "is_public"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "activities", ["catalog_id"], :name => "index_activities_on_catalog_id"
  add_index "activities", ["user_id"], :name => "index_activities_on_user_id"

  create_table "catalogs", :force => true do |t|
    t.integer  "user_id"
    t.string   "title"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "catalogs", ["user_id"], :name => "index_catalogs_on_user_id"

  create_table "grades", :force => true do |t|
    t.integer  "user_id"
    t.integer  "participation_id"
    t.integer  "item_id"
    t.integer  "score"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "grades", ["item_id"], :name => "index_grades_on_item_id"
  add_index "grades", ["participation_id"], :name => "index_grades_on_participation_id"
  add_index "grades", ["user_id"], :name => "index_grades_on_user_id"

  create_table "items", :force => true do |t|
    t.integer  "catalog_id"
    t.string   "title"
    t.text     "description"
    t.integer  "min_limit"
    t.integer  "max_limit"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "priority"
  end

  add_index "items", ["catalog_id"], :name => "index_items_on_catalog_id"

  create_table "participations", :force => true do |t|
    t.integer  "user_id"
    t.integer  "activity_id"
    t.string   "display_name"
    t.text     "description"
    t.string   "attachment_url"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "participations", ["activity_id"], :name => "index_participations_on_activity_id"
  add_index "participations", ["user_id"], :name => "index_participations_on_user_id"

  create_table "users", :force => true do |t|
    t.string   "login_name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
